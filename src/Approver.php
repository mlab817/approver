<?php

namespace Mlab817\Approver;

class Approver
{
    public function hello(string $name)
    {
        return 'Hello '. $name . '!';
    }
}
